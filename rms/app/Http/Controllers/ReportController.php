<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ReportController extends Controller
{
    /**
     * @var array list of available report documents
     */
    protected $reports = [
        'appointments_issued' => 'CS Form No. 2 Report on  Appointments Issued',
        'erasures_alteration' => 'CS Form No. 3 Certificate of  Erasures Alteration',
        'absence_qualified_eligible' => 'CS Form No. 5 Certification of the  Absence of a Qualified Eligible',
        'dibar' => 'CS Form No. 8 Report on DIBAR',
        'publication_vacant_position' => 'CS Form No. 9 Request for Publication of Vacant Positions',
        'resignation_acceptance' => 'CS Form No. 10 Acceptance of  Resignation',
        'oath_office' => 'CS Form No. 32 Oath of Office',
        'appointment_form_regulated' => 'CS Form No. 33-A Appointment Form - Regulated',
        'medical_certificate' => 'CS Form No. 211 Medical Certificate',
        'personal_data' => 'CS Form No. 212 Personal Data Sheet',
        'position_description' => 'DBM-CSC Form No. 1 Position Description Forms',
    ];

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [

    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Reports');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::all('id', 'title')->pluck('title', 'id');
        $applicants = DB::table('applicants')
            ->select(DB::raw('CONCAT(`first_name`, " " ,`last_name`) as fullname, id'))
            ->get()->pluck('fullname', 'id')->toArray();

        return view('report.index', [
            'reports' => $this->reports,
            'jobs' => $jobs,
            'applicants' => $applicants,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function appointmentIssued(Request $request){
        return view('report.appointments_issued');
    }

    public function erasureAlteration(Request $request){
        return view('report.erasures_alteration');
    }

    public function absenceQualifiedEligible(Request $request){
        return view('report.absence_qualified_eligible');
    }

    public function dibarReport(Request $request){
        return view('report.dibar');
    }

    public function vacantPosition(Request $request){
        return view('report.publication_vacant_position');
    }

    public function resignationAcceptance(Request $request){
        return view('report.resignation_acceptance');
    }

    public function oathOffice(Request $request){
        return view('report.oath_office');
    }

    public function appointmentFormRegulated(Request $request){
        return view('report.appointment_form_regulated');
    }

    public function medicalCertificate(Request $request){
        return view('report.medical_certificate');
    }
}
