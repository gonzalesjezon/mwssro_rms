<?php

namespace App\Http\Controllers;

use App\Applicant,
    App\Countries,
    App\Job,
    App\Education,
    Illuminate\Support\Facades\View,
    Illuminate\Http\Request;

class ApplicantController extends Controller
{
    /**
     * @var array list of publication modes, "where did you find this posting"
     */
    protected $publication = [
        'agency' => 'Agency Web Site',
        'csc_bulletin' => 'CSC Bulletin of Vacant Position',
        'newspaper' => 'Newspaper',
        'others' => 'Others',
    ];

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'first_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'middle_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'last_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'email_address' => 'required|email'
    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Applicants');
        $this->middleware('auth', [
            'except' => ['create', 'store']
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 100;

        if (!empty($keyword)) {
            $applicants = Applicant::latest()->paginate($perPage);
        } else {
            $applicants = Applicant::latest()->paginate($perPage);
        }

        return view('applicant.index', compact('applicants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $applicant = new Applicant();
        $applicant->job_id = (null !== $request->id) ? $request->id : null;

        $countries = Countries::all('code', 'name')
            ->pluck('name', 'code')->toArray();
        $jobs = Job::all('id', 'title')
            ->pluck('title', 'id')->toArray();

        if (!empty($applicant->job_id) && empty($jobs[$applicant->job_id])) {
            abort(404, 'Forbidden not found.');
        }

        return view('applicant.create')->with([
            'action' => 'ApplicantController@store',
            'applicant' => $applicant,
            'civilStatus' => Applicant::getCivilStatus(),
            'gender' => Applicant::getGender(),
            'countries' => $countries,
            'jobs' => $jobs,
            'publication' => $this->publication

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->validationRules);

        $applicant = new Applicant;
        $applicant->fill($request->all());
        $applicant->reference_no = uniqid();
        $applicant->saveImageFileNames($request);
        $applicant->saveDocumentFileNames($request);
        $applicant->setIsFilipino($request->filipino);
        $applicant->setIsNaturalized($request->naturalized);
        $applicant->created_by = (\Auth::id()) ? \Auth::id() : 88888888;

        if ($applicant->save()) {
            $applicant->uploadImageFiles($request);
            $applicant->uploadDocumentFiles($request);
        }

        if (\Auth::check()) {
            return redirect('/applicant')->with('success', 'Successfully applied to vacant position.');
        } else {
            return redirect('/careers')->with('success', 'Successfully applied to vacant position.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        $applicant = Applicant::findOrFail($id);

        return view('applicant.show', [
            'applicant' => $applicant,
            'documentView' => $request->document
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $applicant = Applicant::findOrFail($id);
        // $education = Education::where('applicant_id',$id)->getModels();

        $countries = Countries::all('code', 'name')
            ->pluck('name', 'code')->toArray();
        $jobs = Job::all('id', 'title')
            ->pluck('title', 'id')->toArray();

        return view('applicant.edit')->with([
            'action' => 'ApplicantController@update',
            'applicant' => $applicant,
            'civilStatus' => Applicant::getCivilStatus(),
            'gender' => Applicant::getGender(),
            'countries' => $countries,
            'jobs' => $jobs,
            // 'education' => $education,
            'publication' => $this->publication,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, $this->validationRules);

        $applicant = Applicant::findOrFail($id);
        $oldAttributes = $applicant->getAttributes();
        $applicant->fill($request->all());

        // update media names on db
        $applicant->saveImageFileNames($request, $oldAttributes);
        $applicant->saveDocumentFileNames($request, $oldAttributes);

        $applicant->setIsFilipino($request->filipino);
        $applicant->setIsNaturalized($request->naturalized);
        $applicant->updated_by = \Auth::id();

        if ($applicant->update()) {
            $applicant->deleteMediaFiles($request, $oldAttributes);
            $applicant->uploadImageFiles($request, $oldAttributes);
            $applicant->uploadDocumentFiles($request, $oldAttributes);
            $primary = $this->savePrimaryEduc($request->primary);
        }

        return redirect()->route('applicant.edit', ['id' => $applicant->id])->with('success',
            'Applicant post updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $applicant = Applicant::findOrFail($id);
        $applicant->deleteAllMediaFiles($applicant->getAttributes());
        Applicant::destroy($id);

        return redirect('/applicant')->with('success', 'Applicant data deleted!');
    }

    public function savePrimaryEduc($primary){

        $education = new Education;
        $education->applicant_id        = $primary[0]['applicant_id'];
        $education->school_name         = $primary[0]['school_name'];
        $education->course              = $primary[0]['course'];
        $education->attendance_from     = $primary[0]['attendance_from'];
        $education->attendance_to       = $primary[0]['attendance_to'];
        $education->level               = $primary[0]['level'];
        $education->graduated           = $primary[0]['graduated'];
        $education->awards              = $primary[0]['awards'];
        $education->educ_level          = 1;
        $education->save();
    }

    public function saveSecondaryEduc($secondary){

        foreach ($secondary as $key => $value) {
            $education = new Education;
            $education->applicant_id        = $secondary['applicant_id'];
            $education->school_name         = $secondary['school_name'];
            $education->course              = $secondary['course'];
            $education->attendance_from     = $secondary['attendance_from'];
            $education->attendance_to       = $secondary['attendance_to'];
            $education->level               = $secondary['level'];
            $education->graduated           = $secondary['graduated'];
            $education->awards              = $secondary['awards'];
            $education->educ_level          = 2;
            $education->save();
        }

    }
}
