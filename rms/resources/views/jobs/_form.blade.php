@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'job-form']) !!}
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    {{ Form::label('status', 'Status', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('status', $job->status, [
                'class' => 'form-control',
                'placeholder' => 'status',
                'required' => true,
                'disabled' => true,
            ])
        }}
        {{ Form::hidden('status', $job->status) }}
        {!! $errors->first('status', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('title') ? 'has-error' : ''}}">
    {{ Form::label('title', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::text('title', $job->title, [
            'class' => 'form-control',
            'placeholder' => 'title',
            'required' => true,
        ])
        }}
        {!! $errors->first('title', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

@if($status == 'plantilla')
    <div class="form-group row {{ $errors->has('grade') ? 'has-error' : ''}}">
        {{ Form::label('grade', 'Job Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('grade', $job->grade, [
                'class' => 'form-control',
                'placeholder' => 'Job Grade',
                'required' => true,
            ])
            }}
            {!! $errors->first('grade', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>


    <div class="form-group row {{ $errors->has('plantilla_item_number') ? 'has-error' : ''}}">
        {{ Form::label('plantilla_item_number', 'Plantilla Item No', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('plantilla_item_number', $job->plantilla_item_number, [
                'class' => 'form-control',
                'placeholder' => 'item number',
                'required' => true,
            ])
            }}
            {!! $errors->first('plantilla_item_number', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>

    <div class="form-group row {{ $errors->has('annual_basic_salary') ? 'has-error' : ''}}">
        {{ Form::label('annual_basic_salary', 'Annual Basic Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-xl-6 col-sm-8 col-lg-6">
            {{ Form::text('annual_basic_salary', $job->annual_basic_salary, [
                'class' => 'form-control',
                'placeholder' => 'PHP 0',
                'required' => true
            ])
            }}
            {!! $errors->first('annual_basic_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
@endif

@if($status == 'non-plantilla')
    <div class="form-group row {{ $errors->has('daily_salary') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
        {{ Form::label('daily_salary', 'Daily Salary', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('daily_salary', $job->daily_salary, [
                'class' => 'form-control',
                'placeholder' => 'Daily Salary',
                'required' => false,
            ])
            }}
            {!! $errors->first('daily_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
@endif

<div class="form-group row {{ $errors->has('description') ? 'has-error' : ''}}">
    {{ Form::label('description', 'Position Description', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-6">
        <div id="description" name="description"></div>
        {{ Form::textarea('description', $job->description,['id'=>'description-text', 'class'=>'d-none']) }}
        {!! $errors->first('description', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('division') ? 'has-error' : ''}}">
    {{ Form::label('division', 'Office Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-12 col-sm-8 col-lg-6">
        {{ Form::select('division', $division, $job->division, [
        'class' => 'form-control',
        'placeholder' => 'Choose Division',
        'required' => true,
        ])
        }}
        {!! $errors->first('division', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('education') ? 'has-error' : ''}}">
    {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-6">
        <div id="education" name="education"></div>
        {{ Form::textarea('education', $job->education,['id'=>'education-text', 'class'=>'d-none']) }}
        {!! $errors->first('education', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('experience') ? 'has-error' : ''}}">
    {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-6">
        <div id="experience" name="experience"></div>
        {{ Form::textarea('experience', $job->experience,['id'=>'experience-text', 'class'=>'d-none']) }}
        {!! $errors->first('experience', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('training') ? 'has-error' : ''}}">
    {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-6">
        <div id="training" name="training"></div>
        {{ Form::textarea('training', $job->training,['id'=>'training-text', 'class'=>'d-none']) }}
        {!! $errors->first('training', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('eligibility') ? 'has-error' : ''}}">
    {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
    <div class="col-6">
        <div id="eligibility" name="eligibility"></div>
        {{ Form::textarea('eligibility', $job->eligibility,['id'=>'eligibility-text', 'class'=>'d-none']) }}
        {!! $errors->first('eligibility', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>

@if($status == 'non-plantilla')
    <div class="form-group row {{ $errors->has('duties_responsibilities') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
        {{ Form::label('duties_responsibilities', 'Duties and Responsibilities', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-6">
            <div id="duties_responsibilities" name="duties_responsibilities"></div>
            {{ Form::textarea('duties_responsibilities', $job->duties_responsibilities,['id'=>'duties_responsibilities-text', 'class'=>'d-none']) }}
            {!! $errors->first('duties_responsibilities', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>

    <div class="form-group row {{ $errors->has('key_competencies') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
        {{ Form::label('key_competencies', 'Key Competencies', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-6">
            <div id="key_competencies" name="key_competencies"></div>
            {{ Form::textarea('key_competencies', $job->key_competencies,['id'=>'key_competencies-text', 'class'=>'d-none']) }}
            {!! $errors->first('key_competencies', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>

    <div class="form-group row {{ $errors->has('requirements') ? 'has-error' : ''}} {{ ($status == 'plantilla') ? 'd-none' : '' }}">
        {{ Form::label('requirements', 'Requirements', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-6">
            <div id="requirements" name="requirements"></div>
            {{ Form::textarea('requirements', $job->requirements,['id'=>'requirements-text', 'class'=>'d-none']) }}
            {!! $errors->first('requirements', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
@endif

@if(false)
    <div class="form-group row {{ $errors->has('expires') ? 'has-error' : ''}}">
        {{ Form::label('expires', 'Expires', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{$job->expires}}
            <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                {{ Form::text('expires', $job->expires, [
                'class' => 'form-control',
                'placeholder' => 'Expires',
                'required' => true,
                ])
                }}
                <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                </div>
            </div>
            {!! $errors->first('division', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
        </div>
    </div>
@endif

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-sm-right">Publish</label>
    <div class="col-12 col-sm-8 col-lg-6 pt-1">
        <div class="switch-button switch-button-success switch-button-yesno">
            <input type="checkbox" name="publish" id="swt8" {{ $job->publish ? 'checked' : '' }}><span>
                                <label for="swt8"></label></span>
        </div>
    </div>
</div>

<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('jobs._form-script')
@endsection
