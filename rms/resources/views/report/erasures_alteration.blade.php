@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div id="reports" style="width: 960px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-2">
		<div class="col-sm-12">
			CS Form No. 3 <br>
			Series of 2017
		</div>
	</div>

	<div class="row mb-6">
		<div class="col-sm-12 text-center">
			<h4><b>Republic of the Philippines</b></h4>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-12 text-center">
			<h4><b>CERTIFICATION of ERASURE(S)/ALTERATION(S) on APPOINTMENT</b></h4>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<p style="text-indent: 30px;">This is to certify that the appointment of Mr/Ms ________________________ <br>contains the following erasure(s)/alteration(s):</p>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-sm-12">
			<table class="table table-striped table-hover table-fw-widget table-bordered">
				<thead>
					<tr class="text-center">
						<th rowspan="2" style="vertical-align: middle;">PARTICULARS</th>
						<th colspan="2">ERASURE(S)/ALTERATION(S) MADE</th>
					</tr>
					<tr class="text-center font-weight-bold">
						<td>FROM</td>
						<td>TO</td>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-12">
			<p style="text-indent: 30px;" >The erasure(s)/alteration(s) listed above is/are duly authorized and the initial(s) thereon is/are hereby  authenticated, pursuant to Section 2 (a), Rule II of CSC  Memorandum No. ___, s. 2017.</p>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-8"></div>
		<div class="col-sm-4 text-center">
			<hr>
			Appointing Officer / Authority
		</div>
	</div>

	<div class="row mb-6">
		<div class="col-sm-4 text-center">
			<hr>
			Date: _________________
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-12"><h4><b>INSTRUCTIONS</b></h4></div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-12">
			<ol>
				<li>This form is required when there are erasures or alterations made on the appointment form.</li>
				<li>Use the table provided to indicate the changes made in the appointment, and put “Nothing follows” below the last erasure/alteration indicated in the table. Additional rows may be inserted as needed</li>
				<li>
					The appointing officer/authority must first verify the authenticity of the erasure(s)/alteration(s) stated therein, and that “Nothing follows” is indicated after the last erasure(s)/alteration(s) so that no further items  may be added after the certification has been signed.
				</li>
				<li>
					This form shall be submitted together with the required forms within thirty (30) calendar days from date of issuance of appointment for regulated agencies and within the 30th day of the succeeding month for accredited/deregulated agencies.
				</li>
			</ol>
		</div>
	</div>
</div>

 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection