@extends('layouts.app')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Reports</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
          </div>
          <div class="card-body p-2">
            <div class="form-group row">
              {{ Form::label('reports_name', 'Reports Name', [
                  'class'=>'col-12 col-sm-3 col-md-1 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('reports_name', $reports, '', [
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Reports Name',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="form-group row">
              {{ Form::label('position', 'Position', [
                  'class'=>'col-12 col-sm-3 col-md-1 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('name', $jobs, '', [
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Position',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="form-group row">
              {{ Form::label('applicant_name', 'Name', [
                  'class'=>'col-12 col-sm-3 col-md-1 col-form-label text-sm-right'
               ])}}
              <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('name', $applicants, '', [
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Name',
                        'required' => true,
                    ])
                }}
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-sm-3 col-md-1 col-form-label text-sm-right"> Date </label>
              <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
                <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                  <input size="16" type="text" value="" name="date"
                         class="form-control form-control-sm"
                         placeholder="Birthday">
                  <div class="input-group-append">
                    <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-12 col-sm-3 col-md-1 col-form-label text-sm-right"> Sex </label>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-female"></span></div><span class="icon-class"> </span>
                </div>
              </div>
              <div class="col-12 col-sm-7 col-md-1 col-lg-1 col-xl-1">
                <div class="icon-container">
                  <div class="icon"><span class="mdi mdi-male"></span></div><span class="icon-class"> </span>
                </div>
              </div>
            </div>

            <hr>

            <div class="form-group row">
              <div class="col-4 offset-6">
                <buton class="btn btn-secondary" id="preview">Preview</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

      var reportName;
      $(document).on('change','#reports_name',function(){
          reportName = $(this).find(':selected').val();
      })

      $(document).on('click','#preview',function(){
          href = window.location+'/'+reportName;
          window.open(href, '_blank');
      });

    });
  </script>
@endsection
