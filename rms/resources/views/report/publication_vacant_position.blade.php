@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-1">
    <div class="col-sm-3">CS Form No. 9 <br> Revised 2017</div>
    <div class="col-sm-6"></div>
    <div class="col-sm-3 text-center">
    	<p class="border p-1">
    		Electronic copy to be submitted to the CSC FO must be in MS Excel format
    	</p>
    </div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-12 text-center">
  		<h4 class="font-weight-bold">Republic of the Philippines</h4>
  		<h4 class="font-weight-bold">(Name of Agency)</h4>
  		<h4 class="font-weight-bold">Request for Publication of Vacant Positions</h4>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<h4 class="font-weight-bold">To: CIVIL SERVICE COMMISSION (CSC)</h4>
  		<p style="text-indent: 70px;">This is to request the publication of the following vacant positions of (Name of Agency) in the CSC website:</p>
  	</div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-8"></div>
  	<div class="col-sm-4 text-center">
  		<hr>
  		(Head of Agency)
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-8"></div>
  	<div class="col-sm-4">
  		Date
  	</div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-12">
  		<table class="table table-striped table-hover table-fw-widget table-bordered">
  			<thead>
  				<tr class="text-center">
  					<th rowspan="2" style="vertical-align: middle;">No.</th>
  					<th rowspan="2" style="vertical-align: middle;">Position Title</th>
  					<th rowspan="2" style="vertical-align: middle;">Plantilla Item No.</th>
  					<th rowspan="2" style="vertical-align: middle;">Annual Salary</th>
  					<th colspan="5">Qualification Standards</th>
  					<th rowspan="2" style="vertical-align: middle;">Place of Assignment</th>
  				</tr>
  				<tr class="text-center">
  					<th style="vertical-align: middle;">Education</th>
  					<th style="vertical-align: middle;">Training</th>
  					<th style="vertical-align: middle;">Experience</th>
  					<th style="vertical-align: middle;">Eligibility</th>
  					<th style="vertical-align: middle;">Competency <br> (if applicable)</th>
  				</tr>
  			</thead>
  		</table>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<p>Interested and qualified applicants should signify their interest in writing. Attach the following documents to the application letter and send to the address below not later _____</p>
  	</div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-12">
  		<ol>
  			<li>Fully accomplished Personal Data Sheet (PDS) with recent passport-sized picture (CS Form No. 212, Revised 2017) which can be downloaded at www.csc.gov.ph;</li>
  			<li>Performance rating  in the present position for one (1) year (if applicable);</li>
  			<li>Photocopy of certificate of eligibility/rating/license; and</li>
  			<li>Photocopy of Transcript of Records.</li>
  		</ol>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<p><b>QUALIFIED APPLICANTS</b> are advised to hand in or send through courier/email their application to:</p>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-3 text-center">
  		<p class="border-bottom">(Head of Office/Agency)</p>
  		<p class="border-bottom">(Position Title)</p>
  		<p class="border-bottom">(Complete Office Address)</p>
  		<p class="border-bottom">(E-mail Address)</p>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<p><b>APPLICATIONS WITH INCOMPLETE DOCUMENTS SHALL NOT BE ENTERTAINED.</b></p>
  	</div>
  </div>

</div>

 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection