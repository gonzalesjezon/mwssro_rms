<!-- Nifty Modal-->
<div id="md-flipH" class="full-width modal-container modal-effect-8">
    <div class="modal-content modal-certificate px-1">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span
                        class="mdi mdi-close"></span></button>
        </div>

        <div id="printThis" class="modal-body border border-dark rounded">
            <div class="row mb-4">
                <div class="col text-center"><h3><strong>CERTIFICATION</strong></h3></div>
            </div>
            <div class="row">
                <div class="col text-center">The PCC HRM Promotion and Selection Board recommended the appointment of
                    <span class="text-uppercase"><u><strong>(Maxine Medina)</strong></u></span> for the position of
                    <br />
                </div>
            </div>
            <div class="row mb-4">
                <div class="col text-left">
                    <strong>
                        <span class="text-uppercase"><u>(Managing Director)</u></span> (Item No.)
                        <span class="text-uppercase"><u>13218-69786</u></span>, (JG-<span
                                class="text-uppercase"><u>20).</u></span>
                    </strong>
                </div>
            </div>

            <div class="row text-center mb-2">
                <div class="col"><strong>PCC HRM PROMOTION AND SELECTION BOARD</strong></div>
            </div>

            <div class="row text-center">
                <div class="col-4 offset-4">
                    {{ Form::text('chairperson', '', [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => '',
                        ])
                    }}
                    <hr>
                    <strong>Chairperson</strong>
                </div>
            </div>

            <div class="row text-center mb-4">
                <div class="col">
                    {{ Form::text('chairperson', '', [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => '',
                        ])
                    }}
                    <hr>
                    Director IV - AO
                </div>
                <div class="col">
                    {{ Form::text('chairperson', '', [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => '',
                        ])
                    }}
                    <hr>
                    Head of Requisitioning Office
                </div>
                <div class="col">
                    {{ Form::text('chairperson', '', [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => '',
                        ])
                    }}
                    <hr>
                    Human Resource Development Division
                </div>
                <div class="col">
                    {{ Form::text('chairperson', '', [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => '',
                        ])
                    }}
                    <hr>
                    EA Representative
                </div>
            </div>

            <div class="row text-left">
                <div class="col"><strong>Prepared by:</strong></div>
            </div>

            <div class="row">
                <div class="col-3">
                    {{ Form::text('chairperson', '', [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => '',
                        ])
                    }}
                </div>
                <div class="col offset-6 modal-buttons">
                    <button type="button" class="btn btn-info btn-space modal-close" id="print-button">
                        <span class="mdi mdi-print"></span> Print
                    </button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger btn-space modal-close">
                        <span class="mdi mdi-close"></span> Close
                    </button>
                </div>
            </div>
        </div>
        <div class="modal-footer"></div>
    </div>
</div>