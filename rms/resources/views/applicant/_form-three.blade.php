    <div class="form-group row">
        {{ Form::label('house_number', 'House Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('house_number', $applicant->house_number, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'House Number',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('street', 'Street', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('street', $applicant->street, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'street',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('subdivision', 'Subdivision', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('subdivision', $applicant->subdivision, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Subdivision',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('barangay', 'Barangay', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('barangay', $applicant->barangay, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Barangay/District',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('city', 'City', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('city', $applicant->city, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'City',
                    'required' => true,
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('province', 'Province', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('province', $applicant->province, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Province',
                    'required' => true,
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('country_id', 'Country', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::select('country', $countries, $applicant->country, [
                    'class' => 'form-control form-control-xs',
                    'placeholder' => 'Country',
                    'required' => true,
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('permanent_house_number', 'Permanent House Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_house_number', $applicant->permanent_house_number, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Permanent House Number',
                ])
            }}
        </div>
    </div>
    <div class="form-group row">
        {{ Form::label('permanent_street', 'Permanent Street Address', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_street', $applicant->permanent_street, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Permanent Street Address',
                ])
            }}
        </div>
    </div>
    <div class="form-group row">
        {{ Form::label('permanent_subdivision', 'Permanent Subdivision', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_subdivision', $applicant->permanent_subdivision, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Permanent Subdivision',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('permanent_barangay', 'Permanent Barangay', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_barangay', $applicant->permanent_barangay, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Permanent Barangay',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('permanent_city', 'Permanent City', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_city', $applicant->permanent_city, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Permanent City',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('permanent_province', 'Permanent Province', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_province', $applicant->permanent_province, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Permanent Province',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('permanent_country_id', 'Permanent Country', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::select('permanent_country', $countries, $applicant->permanent_country, [
                    'class' => 'form-control form-control-xs',
                    'placeholder' => 'Permanent Country',
                ])
            }}
        </div>
    </div>
    <div class="form-group row">
        {{ Form::label('permanent_telephone_number', 'Permanent Telephone Number', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('permanent_telephone_number', $applicant->permanent_telephone_number, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Permanent Telephone Number',
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('remarks', 'Remarks', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
        <div class="col-12 col-sm-8 col-lg-6">
            {{ Form::text('remarks', $applicant->remarks, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Remarks',
                ])
            }}
        </div>
    </div>

    <div class="form-group row text-right">
        <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
            {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space wizard-previous', 'data-wizard' => '#wizard1']) }}
            {{ Form::button('Next Step', ['class'=>'btn btn-primary btn-space wizard-next btn-form-two', 'data-wizard' => '#wizard1']) }}
            {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
        </div>
    </div>