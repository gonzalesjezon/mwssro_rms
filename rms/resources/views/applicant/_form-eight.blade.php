{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
  <div class="col-4 text-center">TITLE OF LEARNING AND DEVELOPMENT INTERVENTIONS/TRAINING PROGRAMS</div>
  <div class="col-2 text-center">INCLUSIVE DATES OF ATTENDANCE</div>
  <div class="col-1 text-center">NUMBER OF HOURS</div>
  <div class="col-2 text-center">TYPE OF LD</div>
  <div class="col-3 text-center">CONDUCTED/SPONSORED BY</div>
</div>
<div class="form-group row">
  <div class="col-4 text-center">
    (Write in full/Do no abbreviate)
    {{ Form::text('training[1][title_learning_programs]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][title_learning_programs]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    FROM
    {{ Form::text('training[1][inclusive_date_from]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    TO
    {{ Form::text('training[1][inclusive_date_to]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold mt-4">
    {{ Form::text('training[1][number_hours]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][number_hours]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center">
    (Managerial/Supervisor/Technical/etc.)
    {{ Form::text('training[1][ld_type]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][ld_type]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    (Write in full/Do no abbreviate)
    {{ Form::text('training[1][sponsored_by]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][sponsored_by]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

{{--2nd Row--}}
<div class="form-group row">
  <div class="col-4 text-center">
    {{ Form::text('training[2][title_learning_programs]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[2][title_learning_programs]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    {{ Form::text('training[2][inclusive_date_from]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[2][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('training[2][inclusive_date_to]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[2][inclusive_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('training[2][number_hours]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[2][number_hours]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center">
    {{ Form::text('training[2][ld_type]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[2][ld_type]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    {{ Form::text('training[2][sponsored_by]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[2][sponsored_by]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

{{--3rd Row--}}
<div class="form-group row">
  <div class="col-4 text-center">
    {{ Form::text('training[3][title_learning_programs]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[3][title_learning_programs]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    {{ Form::text('training[3][inclusive_date_from]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[3][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('training[3][inclusive_date_to]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[3][inclusive_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('training[3][number_hours]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[3][number_hours]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center">
    {{ Form::text('training[3][ld_type]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[3][ld_type]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    {{ Form::text('training[3][sponsored_by]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[3][sponsored_by]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space wizard-previous', 'data-wizard' => '#wizard1']) }}
    {{ Form::submit('Save', ['class'=>'btn btn-space btn-primary']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>