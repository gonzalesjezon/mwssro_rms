{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
  <div class="col-2 offset-1 text-center">NAME OF SCHOOL (Write in Full)</div>
  <div class="col-2 offset-1">DEGREE/COURSE (Write in Full)</div>
  <div class="col-2 text-center">Period of Attendance</div>
  <div class="col-1">Highest Level/Units Earned</div>
  <div class="col-1">Year Graduated</div>
  <div class="col-2">Scholarship/Academic Honors Received</div>
</div>
<div class="form-group row">
  <div class="col-form-label col-1 font-weight-bold mt-4">
    Primary
  </div>
  <input type="hidden" name="primary[0][applicant_id]" value="{{$applicant->id}}">
  <div class="col-3 mt-4">
    {{ Form::text('primary[0][school_name]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('primary[0][course]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    From
    {{ Form::text('primary[0][attendance_from]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    To
    {{ Form::text('primary[0][attendance_to]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('primary[0][level]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('primary[0][graduated]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('primary[0][awards]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

{{--SECONDARY--}}
<div class="form-group row">
  {{ Form::label('primary_name', 'Secondary', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-3">
    {{ Form::text('secondary[0][school_name]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2">
    {{ Form::text('secondary[0][course]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('secondary[0][attendance_from]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('secondary[0][attendance_to]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('secondary[0][level]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('secondary[0][graduated]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2">
    {{ Form::text('secondary[0][awards]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

{{--VOCATIONAL--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_vocational" class="btn btn-sm btn-info">Add</a>
  </div>
</div>
<div class="form-group row vocational">
  {{ Form::label('primary_name', 'Vocational', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-3">
    {{ Form::text('vocational[0][school_name]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('vocational[0][course]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('vocational[0][attendance_from]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('vocational[0][attendance_to]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('vocational[0][level]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('vocational[0][graduated]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('vocational[0][awards]', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
</div>

{{--COLLEGE--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_college" class="btn btn-sm btn-info">Add</a>
  </div>
</div>
<div class="form-group row college">
  {{ Form::label('primary_name', 'College', ['class'=>'col-form-label text-sm-right col']) }}
  <div class="col-3">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
</div>

{{--GRADUATE STUDIES--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_graduate_studies" class="btn btn-sm btn-info">Add</a>
  </div>
</div>
<div class="form-group row graduate-studies">
<!--   <div class="col-form-label text-sm-right col font-weight-bold">
    Graduate Studies
  </div> -->
  <label class="col-form-label col-1 font-weight-bold col">Graduate <br> Studies</label>
  <div class="col-3">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('XXXXX', $applicant->first_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
</div>

<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>