@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
        rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}" />
@endsection

{!! Form::open(['action' => $actionPosition, 'method' => 'GET', 'id' => 'comparative-ranking-form']) !!}

<div class="form-group row">
  {{ Form::label('position_consideration', 'Position', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    {{ Form::select('position_consideration', $jobs, '', [
            'id' => 'position_consideration',
            'class' => 'form-control form-control-xs',
            'placeholder' => 'Vacant Positions',
            'name'=>'position_consideration',
            'required' => true,
        ])
    }}
  </div>
  {{ Form::label('education', 'Education', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-5">
    <div id="education"></div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('grade', 'Job Grade', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value="{{$currentJob->grade}}"
      name="grade"
      class="form-control form-control-sm"
      placeholder="Job Grade"
    >
  </div>
  {{ Form::label('experience', 'Experience', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-5">
    <div id="experience"></div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('plantilla_item_number', 'Item Number', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value="{{$currentJob->plantilla_item_number}}"
      name="plantilla_item_number"
      class="form-control form-control-sm"
      placeholder="Item Number"
    >
  </div>
  {{ Form::label('training', 'Training', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-5">
    <div id="training"></div>
  </div>
</div>

<div class="form-group row">
  {{ Form::label('division_office', 'Division/Office', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value="{{config('params.division.'.$currentJob->division)}}"
      class="form-control form-control-sm"
      placeholder="Division/Office"
      readonly
    >
  </div>
  {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
  <div class="col-5">
    <div id="eligibility"></div>
  </div>
</div>
{!! Form::close() !!}

<table id="table1" class="table table-striped table-hover table-fw-widget table-bordered">
  <thead>
  <tr>
    <th rowspan="2">NAME</th>
    <th colspan="2" scope="colgroup">PERFORMANCE</th>
    <th colspan="3" scope="colgroup">EDUCATION AND TRAINING</th>
    <th colspan="5" scope="colgroup">EXPERIENCE AND OUTSTANDING ACCOMPLISHMENTS</th>
    <th colspan="2" scope="colgroup">PYSCHOSOCIAL</th>
    <th colspan="2" scope="colgroup">POTENTIAL</th>
  </tr>
  <tr>
    <th>POINTS</th>
    <th scope="col">40%</th>
    <th scope="col">POINTS(Education)+</th>
    <th scope="col"><i>POINTS(Training)</i></th>
    <th scope="col">20%</th>
    <th colspan="2" scope="col">POINTS(Relevant)+</th>
    <th scope="col"><i>POINTS <br> (Specialized)</i></th>
    <th scope="col"></th>
    <th scope="col">20%</th>
    <th scope="col">POINTS</th>
    <th scope="col">10%</th>
    <th scope="col">POINTS</th>
    <th scope="col">10%</th>
  </tr>
  </thead>
  <tbody>
  {!! Form::open(['action' => $action, 'method' => $method, 'id' => 'matrix-form']) !!}
  @foreach($evaluations as $key => $evaluation)
    <tr>
      <td>{{$evaluation->applicant->getFullname()}}</td>
      <td>{{$evaluation->performance_score}}</td>
      <td>{{$evaluation->performance_percent}}</td>
      <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
      <td>{{$evaluation->education_training_total_points}}</td>
      <td>{{$evaluation->education_training_score}}</td>
      <td>{{$evaluation->experience_accomplishments_total_points}}</td>
      <td></td>
      <td></td>
      <td></td>
      <td>{{$evaluation->experience_accomplishments_score}}</td>
      <td>{{$evaluation->psychosocial_percentage_rating}}</td>
      <td>{{$evaluation->psychosocial_score}}</td>
      <td>{{$evaluation->psychosocial_percentage_rating}}</td>
      <td>{{$evaluation->psychosocial_score}}</td>
    </tr>
  @endforeach
  </tbody>
</table>

<div class="row mb-1">
  <div class="col">
    Reference (based n Merit Selection Plan):
  </div>
</div>

<div class="row mb-1">
  <div class="col col-lg-1">Performance</div>
  <div class="col col-lg-1">40%</div>
  <div class="col">
    indicates the employee's efficiency in discharging his/her duties and responsibilities.
  </div>
</div>

<div class="row mb-2">
  <div class="col col-lg-1">Education & Training</div>
  <div class="col col-lg-1">20%</div>
  <div class="col">
    includes educational background, trainings/seminars attended relevant to the duties of the position to be filled. To
    enhance the effectiveness of the employee in achieving objectives and goals, PCC gives credit to candidates with
    credentials in higher education. However, only relevant educational degree or units/trainings in excess of minimum
    requirements shall be given extra points. For extra points, only the trainings/seminars for the least five(5) years
    are credited and not to exceed ten(10) points.
  </div>
</div>

<div class="row mb-2">
  <div class="col col-lg-1">Experience</div>
  <div class="col col-lg-1">20%</div>
  <div class="col">
    refers to occupational history, work experience related/relevant to the area of knowledge or activity required for
    the
    position.
  </div>
</div>

<div class="row mb-2">
  <div class="col col-lg-1">Psychosocial Attributes</div>
  <div class="col col-lg-1">10%</div>
  <div class="col">
    refers to the characteristics or traits of a person which involve both physical and social aspects. Pyschological
    attributes include the way the applicant/candidate perceives things, ideas, beliefs and understanding and how the
    applicant/candidate acts and relates them to others and social situations. This shall be measured in the panel
    interview to be conducted by the internal selection committee.
  </div>
</div>

<div class="row mb-2">
  <div class="col col-lg-1">Potential</div>
  <div class="col col-lg-1">10%</div>
  <div class="col">
    refers to the employee's capacity to perform the duties and assume the responsibilities of the position to be filled
    and those of higher positions. This shall be measured in the panel interview to be conducted by the internal
    selection
    committee.
  </div>
</div>

<div class="row mb-2">
  <div class="col">Evaluated By:</div>
</div>

<div class="form-group row text-center">
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value=""
      name="position_consideration"
      class="form-control form-control-sm"
    >
    <hr>
    ISC Chairperson
  </div>
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value=""
      name="division_office"
      class="form-control form-control-sm"
    >
    <hr>
    ISC Member
  </div>
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value=""
      name="division_office"
      class="form-control form-control-sm"
    >
    <hr>
    ISC Member
  </div>
  <div class="col-12 col-sm-8 col-lg-3">
    <input
      size="16"
      type="text"
      value=""
      name="division_office"
      class="form-control form-control-sm"
    >
    <hr>
    EA Representative
  </div>
</div>
{!! Form::close() !!}

@section('scripts')
  <!-- JS Libraries -->
  <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
          type="text/javascript"></script>
  <!-- wysiwyg -->
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.min.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/lib/summernote/summernote-ext-beagle.js') }}"
          type="text/javascript"></script>
  <script src="{{ URL::asset('beagle-assets/js/app-form-wysiwyg.js') }}" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      //initialize the javascript
      App.init();
      App.formElements();

      // instansiate summernote(wysiwyg) editor
      const $education = $('#education');
      const $experience = $('#experience');
      const $eligibility = $('#eligibility');
      const $training = $('#training');

      // place data into summernote(wysiwyg) editor
      $education.summernote('code', `{!! @$currentJob->education !!}`);
      $experience.summernote('code', `{!! @$currentJob->experience !!}`);
      $eligibility.summernote('code', `{!! @$currentJob->eligibility !!}`);
      $training.summernote('code', `{!! @$currentJob->training !!}`);

      // remove tools on summernote(wysiwyg) editor
      $('.note-toolbar').remove();
      $('.note-editable').css('height', '4rem');

      $education.summernote('disable');
      $experience.summernote('disable');
      $eligibility.summernote('disable');
      $training.summernote('disable');

      $education.summernote({ height: 10 });
      $experience.summernote('disable');
      $eligibility.summernote('disable');
      $training.summernote('disable');

      $('#position_consideration').change(function() {
        $('#comparative-ranking-form').submit();
      });
    });
  </script>
@endsection
