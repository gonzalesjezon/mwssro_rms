@section('css')
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}
<div class="row">
    <div class="col text-light bg-secondary">I. PERFORMANCE (40%)</div>
</div>

<div class="form-group row">
    <div class="col">For Transferees:</div>
</div>

<div class="form-group row">
    <div class="col">
        {{ Form::text('performance', $evaluation->performance, [
                'class' => 'form-control form-control-xs performance',
                'id' => 'performance'
            ])
        }}
        <hr>
        <label for="">Rating 1 + Rating 2</label>
    </div>

    <label for=""><h4 class="mt-1">/</h4></label>
    <div class="col">
        {{ Form::text('performance_divide', $evaluation->performance_divide, [
                'class' => 'form-control form-control-xs performance',
                'id' => 'performance_divide'
            ])
        }}
        <hr>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
        {{ Form::text('performance_average', $evaluation->performance_average, [
                'class' => 'form-control form-control-xs performance',
                'id' => 'performance_average'
            ])
        }}
        <hr>
        <label for="">Average Rating</label>
    </div>

    <div class="offset-1"><h4 class="mt-1">X</h4></div>
    <div class="col">
        {{ Form::text('performance_percent', $evaluation->performance_percent, [
                'class' => 'form-control form-control-xs performance',
                'id' => 'performance_percent'
            ])
        }}
        <hr>
        <label for="">POINTS WEIGHT</label>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
        {{ Form::text('performance_score', $evaluation->performance_score, [
                'class' => 'form-control form-control-xs performance',
                'id' => 'performance_score'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    <div class="col">
        <label for="">* For Non-Transferee, a grade of Satisfactory rating is given.</label>
    </div>
</div>

<!-- Education & Training -->
<div class="form-group row">
    <div class="col text-light bg-secondary">II. EDUCATION & TRAINING (20%)</div>
</div>

<div class="form-group row">
    {{ Form::label('eligibility', 'Eligibility', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
        {{ Form::text('eligibility', $evaluation->eligibility, [
                'class' => 'form-control',
                'placeholder' => 'Eligibility',
                'id' => 'eligibility'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('training', 'Training', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
        {{ Form::text('training', $evaluation->training, [
                'class' => 'form-control',
                'placeholder' => 'Training',
                'id' => 'training'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('Seminar', 'Seminar', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
        {{ Form::text('seminar', $evaluation->seminar, [
                'class' => 'form-control',
                'placeholder' => 'Seminar',
                'id' => 'seminar'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('minimum_education_points', 'Minimum Educational Requirement', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('minimum_education_points', $evaluation->minimum_education_points, [
                'class' => 'form-control education_training',
                'placeholder' => 'Minimum Educational Requirement',
                'id' => 'minimum_education_points'
            ])
        }}
    </div>
    <div class="col-2 mt-3">PTS</div>
</div>

<div class="form-group row">
    {{ Form::label('minimum_training_points', 'Minimum Training Requirement', ['class'=>'col-2 mt-2']) }}
    <div class="col-2">
        {{ Form::text('minimum_training_points', $evaluation->minimum_training_points, [
                'class' => 'form-control education_training',
                'placeholder' => 'Minimum Training Requirement',
                'id' => 'minimum_training_points'
            ])
        }}
    </div>
    <div class="col-2 mt-3">PTS</div>
</div>

<div class="form-group row">
    {{ Form::label('ratings_excess', 'Ratings in Excess of the Minimum:', ['class'=>'col-2 mt-3 font-weight-bold']) }}
</div>

<div class="form-group row">
    {{ Form::label('education_points', 'Education', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
        {{ Form::text('education_points', $evaluation->education_points, [
                'class' => 'form-control education_training',
                'id' => 'education_points'
            ])
        }}
        <hr>
        <div>Points Rating</div>
    </div>
</div>

<div class="form-group row">
    {{ Form::label('training_points', 'Training', ['class'=>'col-2 mt-2']) }}
    <label for=""><h4 class="mt-3 font-weight-bold">=</h4></label>
    <div class="col-2">
        {{ Form::text('training_points', $evaluation->training_points, [
                'class' => 'form-control education_training',
                'id' => 'training_points'
            ])
        }}
        <hr>
        <div>Points Rating</div>
    </div>

    <label for="" class="offset-1 mt-3">=</label>
    <div class="col-2">
        {{ Form::text('education_training_total_points', $evaluation->education_training_total_points, [
                'class' => 'form-control',
                'id' => 'education_training_total_points'
            ])
        }}
        <hr>
        <div>Total Points</div>
    </div>

    <div class="mt-3">X</div>
    <div class="col-2">
        {{ Form::text('education_training_percent', $evaluation->education_training_percent, [
                'class' => 'form-control education_training',
                'id' => 'education_training_percent'
            ])
        }}
        <hr>
        <div>POINTS WEIGHT</div>
    </div>

    <label for="" class="mt-3">=</label>
    <div class="col-2">
        {{ Form::text('education_training_score', $evaluation->education_training_score, [
                'class' => 'form-control',
                'id' => 'education_training_score'
            ])
        }}
    </div>
</div>

<!-- Experience & Outstanding Accomplishments -->
<div class="form-group row">
    <div class="col text-light bg-secondary">III. EXPERIENCE & OUTSTANDING ACCOMPLISHMENTS (20%)</div>
</div>

<div class="form-group row">
    {{ Form::label('relevant_positions_held', 'Relevant Positions Held', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
        {{ Form::text('relevant_positions_held', $evaluation->relevant_positions_held, [
                'class' => 'form-control',
                'placeholder' => 'Relevant Positions Held',
                'id' => 'relevant_positions_held'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('minimum_experience_requirement', 'Minimum Experience Requirement', ['class'=>'col-2 mt-2']) }}
    <div class="col-3 offset-1">
        {{ Form::text('minimum_experience_requirement', $evaluation->minimum_experience_requirement, [
                'class' => 'form-control experience_accomplishments',
                'placeholder' => 'Minimum Experience Requirement',
                'id' => 'minimum_experience_requirement'
            ])
        }}
    </div>
    <div class="col-2 mt-3">PTS</div>
</div>

<div class="form-group row">
    {{ Form::label('additional_points', 'Additional Points (in excess of the minimum requirement)', ['class'=>'col-2 mt-2']) }}
    <div class="col-3 offset-1">
        {{ Form::text('additional_points', $evaluation->additional_points, [
                'class' => 'form-control experience_accomplishments',
                'placeholder' => 'Additional Points',
                'id' => 'additional_points'
            ])
        }}
    </div>
    <div class="col-2 mt-3">PTS</div>
</div>

<div class="row">
    <label for="" class="offset-3 mt-3">=</label>
    <div class="col-3">
        {{ Form::text('experience_accomplishments_total_points', $evaluation->experience_accomplishments_total_points, [
                'class' => 'form-control',
                'id' => 'experience_accomplishments_total_points',
            ])
        }}
        <hr>
        <label for="">Total Points</label>
    </div>

    <label for="" class="mt-3">X</label>
    <div class="col-2">
        {{ Form::text('experience_accomplishments_percent', $evaluation->experience_accomplishments_percent, [
                'class' => 'form-control experience_accomplishments',
                'id' => 'experience_accomplishments_percent',
            ])
        }}
        <hr>
        <label for="">POINTS WEIGHT</label>
    </div>

    <label for="" class="mt-3">=</label>
    <div class="col-2">
        {{ Form::text('experience_accomplishments_score', $evaluation->experience_accomplishments_score, [
                'class' => 'form-control',
                'id' => 'experience_accomplishments_score',
            ])
        }}
    </div>
</div>

<!-- Potential -->
<div class="form-group row">
    <div class="col text-light bg-secondary">IV. POTENTIAL (10%)</div>
</div>

<div class="form-group row">
    <div class="col">
        {{ Form::text('potential', $evaluation->potential, [
                'class' => 'form-control form-control-xs',
                'id' => 'potential',
            ])
        }}
        <hr>
        <label for="potential">R1 + R2 + R3 + R4 + R5</label>
    </div>

    <div class="col">
        {{ Form::text('potential_average_rating', $evaluation->potential_average_rating, [
                'class' => 'form-control form-control-xs',
                'id' => 'potential_average_rating',
            ])
        }}
        <hr>
        <label for="potential_average_rating">Average Rating</label>
    </div>

    <div class="col">
        {{ Form::text('potential_percentage_rating', $evaluation->potential_percentage_rating, [
                'class' => 'form-control form-control-xs potential',
                'id' => 'potential_percentage_rating'
            ])
        }}
        <hr>
        <label for="">Percentage Rating</label>
    </div>

    <label for="" class="offset-1"><h4 class="mt-1">X</h4></label>
    <div class="col">
        {{ Form::text('potential_percent', $evaluation->potential_percent, [
                'class' => 'form-control form-control-xs potential',
                'id' => 'potential_percent'
            ])
        }}
        <hr>
        <label for="">POINTS WEIGHT</label>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
        {{ Form::text('potential_score', $evaluation->potential_score, [
                'class' => 'form-control form-control-xs',
                'id' => 'potential_score'
            ])
        }}
    </div>
</div>

<!-- Potential -->
<div class="form-group row">
    <div class="col text-light bg-secondary">V. PSYCHOSOCIAL ATTRIBUTES & PERSONALITY TRAITS (10%)</div>
</div>

<div class="form-group row">
    <div class="col">
        {{ Form::text('psychosocial', $evaluation->psychosocial, [
                'class' => 'form-control form-control-xs',
                'id' => 'psychosocial'
            ])
        }}
        <hr>
        <label for="psychosocial">R1 + R2 + R3 + R4 + R5</label>
    </div>

    <div class="col">
        {{ Form::text('psychosocial_average_rating', $evaluation->psychosocial_average_rating, [
                'class' => 'form-control form-control-xs',
                'id' => 'psychosocial_average_rating'
            ])
        }}
        <hr>
        <label for="psychosocial_average_rating">Average Rating</label>
    </div>

    <div class="col">
        {{ Form::text('psychosocial_percentage_rating', $evaluation->psychosocial_percentage_rating, [
                'class' => 'form-control form-control-xs psychosocial',
                'id' => 'psychosocial_percentage_rating'
            ])
        }}
        <hr>
        <label for="">Percentage Rating</label>
    </div>

    <label for="" class="offset-1"><h4 class="mt-1">X</h4></label>
    <div class="col">
        {{ Form::text('psychosocial_percent', $evaluation->psychosocial_percent, [
                'class' => 'form-control form-control-xs psychosocial',
                'id' => 'psychosocial_percent'
            ])
        }}
        <hr>
        <label for="">POINTS WEIGHT</label>
    </div>

    <label for=""><h4 class="mt-1">=</h4></label>
    <div class="col">
        {{ Form::text('psychosocial_score', $evaluation->psychosocial_score, [
                'class' => 'form-control form-control-xs psychosocial',
                'id' => 'psychosocial_score'
            ])
        }}
    </div>
</div>

<hr>

<div class="form-group row">
    {{ Form::label('evaluated_by', 'Evaluated By', ['class'=>'col-2 mt-2']) }}
    <div class="col-4 mr-6">
        {{ Form::text('evaluated_by', $evaluation->evaluated_by, [
                'class' => 'form-control',
                'placeholder' => 'Evaluated By',
                'id' => 'evaluated_by'
            ])
        }}
    </div>

    <div class="col offset-1 ml-5"><h4 class="mt-1">TOTAL</h4></div>
    <div class="col-2">
        {{ Form::text('total_percent', $evaluation->total_percent, [
                'class' => 'form-control form-control-xs',
                'id' => 'total_percent'
            ])
        }}
    </div>

    <div class="col-2">
        {{ Form::text('total_score', $evaluation->total_score, [
                'class' => 'form-control form-control-xs',
                'id' => 'total_score'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('reviewed_by', 'Reviewed By', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
        {{ Form::text('reviewed_by', $evaluation->reviewed_by, [
                'class' => 'form-control',
                'placeholder' => 'Reviewed By',
                'id' => 'reviewed_by'
            ])
        }}
    </div>
</div>

<div class="form-group row">
    {{ Form::label('noted_by', 'Noted By', ['class'=>'col-2 mt-2']) }}
    <div class="col-4">
        {{ Form::text('noted_by', $evaluation->noted_by, [
                'class' => 'form-control',
                'placeholder' => 'Noted By',
                'id' => 'noted_by'
            ])
        }}
    </div>
</div>

{{ Form::hidden('reference', app('request')->input('reference')) }}

<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
        {{ Form::button('Save', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}


<!-- Nifty Modal-->
<div id="md-flipH" class="modal-container modal-effect-8">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span
                        class="mdi mdi-close"></span></button>
        </div>
        <div id="printThis" class="modal-body">
            <img src="{{ URL::asset('img/merit-selection-plan.png') }}" class="img-fluid" />
        </div>
        <div class="modal-footer">
            <div class="mt-8">
                <button type="button" class="btn btn-info btn-space modal-close" id="print-button">
                    <span class="mdi mdi-print"></span> Print
                </button>
                <button type="button" data-dismiss="modal" class="btn btn-danger btn-space modal-close">
                    <span class="mdi mdi-close"></span> Close
                </button>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    @include('evaluation._form-rating-scripts')
@endsection
