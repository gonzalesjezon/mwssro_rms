@extends('layouts.print')

@section('css')
@endsection

@section('content')
  <div class="row mb-4">
    <div class="col-sm-3"><img src="{{URL::asset('img/pcc-logo-small.png')}}" class="img-fluid" alt="pcc logo" /></div>
  </div>

  <div class="dropdown-divider"></div>
  <div class="row mb-4 text-center">
    <div class="col-sm-12"><h2><b>MATRIX OF QUALIFICATIONS</b></h2></div>
  </div>

  <div class="row mb-1">
      <div class="col-2"><b>Position for Consideration:</b></div>
      <div class="col-2"><b>{{ $jobs->title }}</b></div>
      <div class="col-4 offset-1 text-right"><b>Office</b></div>
      <div class="col-2"><b>{{ config('params.division.'.@$jobs->division)}}</b></div>
  </div>

  <div class="row mb-1">
      <div class="col-2"><b>Education:</b></div>
      <div class="col-2"><b>{{ $jobs->education }}</b></div>
  </div>

  <div class="row mb-1">
      <div class="col-2"><b>Experience:</b></div>
      <div class="col-2"><b>{{ $jobs->experience }}</b></div>
  </div>

  <div class="row mb-1">
      <div class="col-2"><b>Training:</b></div>
      <div class="col-2"><b>{{ $jobs->training }}</b></div>
  </div>

  <div class="row mb-1">
      <div class="col-2"><b>Eligibility:</b></div>
      <div class="col-2"><b>{{ $jobs->eligibility }}</b></div>
  </div>

  <div class="row mb-1">
      <div class="col-sm-12">
        <table id="table1" class="table table-striped table-hover table-fw-widget table-bordered">
          <thead>
            <tr class="text-center">
              <th>Name of Applicant Current Position</th>
              <th>Age</th>
              <th>Educational Background</th>
              <th>Work Experience</th>
              <th>Eligibility</th>
              <th>Training</th>
              <th>Remarks</th>
            </tr>
          </thead>
          <tbody>
            @foreach($applicants as $key => $value)
            <tr>
              <td>{{ $value->getFullName() }} </td>
              <td class="text-center">{{ Carbon\Carbon::today()->diffInYears($value->birthday) }}</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
  </div>

  <div class="row mb-4">
      <div class="col-12 text-center"><h5>INTERNAL SELECTION COMMITTEE</h5></div>
  </div>

 <div class="form-group row text-center">
  <div class="col-3">
    <hr>
    ISC Chairperson
  </div>
  <div class="col-3">
    <hr>
    ISC Member
  </div>
  <div class="col-3">
    <hr>
    ISC Member
  </div>
  <div class="col-3">
    <hr>
    EA Representative
  </div>
</div>

  <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection