@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Evaluation</h2>
    </div>

    <!-- Matrix Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle" style="display: initial !important;">You can create matrix of qualification.</span>
                    <a href="{{ route('evaluation.matrix-report',['currentJob' => $currentJob ]) }}"
                        target="_blank"
                        class="btn btn-space btn-warning"
                        style="float: right;"
                        title="View">
                        <i class="icon icon-left mdi mdi-eye"></i> View
                    </a>
                </div>
                <div class="card-body">
                    @include('evaluation._matrix-form', [
                        'action' => $action,
                        'actionPosition' => $actionPosition,
                        'method' => 'POST',
                        'jobs' => $jobs,
                        'currentJob' => $currentJob,
                        'applicants' => $applicants,
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
