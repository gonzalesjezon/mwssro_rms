<?php

/**
 * Public Routes
 *
 */
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/menu', 'HomeController@index')->name('menu');
Route::get('/careers', 'HomeController@careers')->name('careers');
Auth::routes();

/**
 * Routes that require user to be logged in
 * filtered by middleware auth in controller
 */
Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/non-plantilla', 'JobsController@nonPlantilla')->name('jobs.nonplantilla');
Route::get('evaluation/rating', 'EvaluationController@rating')->name('evaluation.rating');
Route::get('jobs/publish', 'JobsController@publish')->name('jobs.publish');
Route::get('evaluation/matrix-qualification', 'EvaluationController@matrixQualification')->name('evaluation.matrix');
Route::get('evaluation/comparative-ranking', 'EvaluationController@comparativeRanking')->name('evaluation.comparative');
Route::get('evaluation/report', 'EvaluationController@evaluationReport')->name('evaluation.report');
Route::get('evaluation/matrix-report', 'EvaluationController@matrixQualificationReport')->name('evaluation.matrix-report');
Route::get('evaluation/comparative-report', 'EvaluationController@comparativeReport')->name('evaluation.comparative-report');
Route::get('report/appointments_issued', 'ReportController@appointmentIssued')->name('report.appointments_issued');
Route::get('report/erasures_alteration', 'ReportController@erasureAlteration')->name('report.erasures_alteration');
Route::get('report/absence_qualified_eligible', 'ReportController@absenceQualifiedEligible')->name('report.absence_qualified_eligible');
Route::get('report/dibar', 'ReportController@dibarReport')->name('report.dibar');
Route::get('report/publication_vacant_position', 'ReportController@vacantPosition')->name('report.publication_vacant_position');
Route::get('report/resignation_acceptance', 'ReportController@resignationAcceptance')->name('report.resignation_acceptance');
Route::get('report/oath_office', 'ReportController@oathOffice')->name('report.oath_office');
Route::get('report/appointment_form_regulated', 'ReportController@appointmentFormRegulated')->name('report.appointment_form_regulated');
Route::get('report/medical_certificate', 'ReportController@medicalCertificate')->name('report.medical_certificate');

Route::post('evaluation/store-matrix-qualification',
    'EvaluationController@storeMatrixQualification')->name('evaluation.storeMatrix');

Route::post('evaluation/store-comparative-ranking',
    'EvaluationController@storeComparativeRanking')->name('evaluation.storeComparative');

Route::resources([
    'applicant' => 'ApplicantController',
    'config' => 'ConfigController',
    'evaluation' => 'EvaluationController',
    'jobs' => 'JobsController',
    'recommendation' => 'RecommendationController',
    'appointment' => 'AppointmentController',
    'joboffer' => 'JobOfferController',
    'assumption' => 'AssumptionController',
    'attestation' => 'AttestationController',
    'report' => 'ReportController',
]);

